import java.util.Random;

public class laba2 {
    public static void main(String[] args) {
        int [] arr = new int[2500];
        int [] arr2 = new int[2500];
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(1000000);
            arr2[i] = random.nextInt(1000000);
        }
        arr = SortVybor(arr).clone();
        arr2 = SortVstavka(arr2).clone();
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + " ");
        }
    }

    public static int[] SortVybor(int[] arr) {
        Long start = System.currentTimeMillis();
        for (int i = 0; i < arr.length; i++) {
            int min = arr[i];
            int min_i = i;
            for (int j = i+1; j < arr.length; j++) {
                if (arr[j] < min) {
                    min = arr[j];
                    min_i = j;
                }
            }
            if (i != min_i) {
                int tmp = arr[i];
                arr[i] = arr[min_i];
                arr[min_i] = tmp;
            }
        }
        Long end = System.currentTimeMillis();
        System.out.println("Методом выбора прошло: " + (end - start) + " миллисекунд");
        return arr;
    }

    public static int[] SortVstavka(int[] arr) {
        Long start = System.currentTimeMillis();
        for (int i = 1; i < arr.length; i++){
            for (int j = i; j > 0 && arr[j-1] > arr[j]; j--){
                int tmp = arr[j-1];
                arr[j-1] = arr[j];
                arr[j] = tmp;
            }
        }
        Long end = System.currentTimeMillis();
        System.out.println("Методом вставки прошло: " + (end - start) + " миллисекунд");
        return arr;
    }
}




