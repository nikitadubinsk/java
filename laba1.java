import java.util.Random;
import java.util.Scanner;

public class laba1 {
    public static void main(String[] args) {
        Test test = new Test(7);
        Random random = new Random();
        for (int i=0; i<6; i++) {
            test.insert(random.nextInt(100));
        }
        test.display();
        System.out.println("Введите искомое число");
        Scanner scanner = new Scanner(System.in);
        Long num = scanner.nextLong();
        if (test.find(num)) System.out.println("Такое число есть");
        else System.out.println("Такого числа нет");

        System.out.println("Ввидите число, которое хотите добавить");
        Long num2 = scanner.nextLong();
        test.insert(num2);
        test.display();

        System.out.println("Введите число, которое хотите удалить");
        Long num1 = scanner.nextLong();
        if (test.delete(num1)) System.out.println("Число удалено");
            else System.out.println("Число не удалено");

        test.display();

    }
}

    class Test {
        private long[] a;
        private int nElems;

        public Test(int max) {
            a = new long[max];
            nElems = 0;
        }

        public boolean find(long searchKey) {
            int j;
            for(j=0; j<nElems; j++)
                if(a[j] == searchKey) break;
            if(j == nElems) return false;
            else return true;
        }

        public void insert(long value) {
            a[nElems] = value;
            nElems++;
        }

        public boolean delete(long value) {
            int j;
            for(j=0; j<nElems; j++) if( value == a[j] )
                break;
            if(j==nElems) return false;
            else {
                for(int k=j; k<nElems-1; k++)
                    a[k] = a[k+1];
                nElems--;
                return true;
            }
        }

        public void display() {
            for(int j=0; j<nElems; j++)
                System.out.print(a[j] + " ");
            System.out.println("");
        }
}
