import java.util.*;

public class laba3 {
    public static void main(String[] args) {
        System.out.println("Введите постфиксную запись, а я посчитаю");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String[] arr = s.split(" ");
        ArrayDeque<Double> Stack = new ArrayDeque<Double>();
            for (int i = 0; i < arr.length; i++) {
                double x = 0;
                double y = 0;
                double r = 0;
                if (isDigit(arr[i])) {
                    Stack.addLast(Double.parseDouble(arr[i]));
                } else
                    switch (arr[i]) {
                        case "+": {
                            x = Stack.removeLast();
                            y = Stack.removeLast();
                            r = x + y;
                            Stack.addLast(r);
                        }
                        break;
                        case "-": {
                            x = Stack.removeLast();
                            y = Stack.removeLast();
                            r = y - x;
                            Stack.addLast(r);
                        }
                        break;
                        case "*": {
                            x = Stack.removeLast();
                            y = Stack.removeLast();
                            r = x * y;
                            Stack.addLast(r);
                        }
                        break;
                        case "/": {
                            x = Stack.removeLast();
                            y = Stack.removeLast();
                            r = y / x;
                            Stack.addLast(r);
                        }
                        break;
                    }
            }
        double a = Stack.getLast();
        System.out.println("Ответ: " + a);
    }

    private static boolean isDigit(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
